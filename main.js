$('*').each(function(){
    // use .data instead of .attr, 
    // or ping the server with selector
    // using .attr because I want to visually see changes
   $(this).attr("octomap-click", 0); 
   $(this).attr("octomap-hover", 0); 
    
   $(this).click(function(e){
       let clickCount = $(e.target).attr("octomap-click");
       $(e.target).attr("octomap-click", +clickCount+1);
       e.stopImmediatePropagation();
   });
    
   $(this).mouseenter(function(e){
       let clickCount = $(e.target).attr("octomap-hover");
       $(e.target).attr("octomap-hover", +clickCount+1);
   });
});
